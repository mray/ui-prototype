# HTML/CSS Style Guide

>Note: this is a draft.

## General

- Where possible, please keep line width to 79 columns.
    - Reason: better viewing experience on smaller screens, allows having
      multiple files open side by side on wider screens.

## Hamlet

- Indentation: 2 spaces
    - Reason: Hamlet syntax relies heavily on indentation for correct tag
      closure, resulting in more indent levels in some cases than plain
      HTML. An indentation of 2 spaces leaves more room for long lines and
      paragraphs.

- After it is compiled, check the HTML output for correct tag closure.
    - Reason: incorrect tag closure can affect CSS styling. For example,
      incorrect tag closure may be caused by unescaped line endings in links.

## Sass

- Indentation: 4 spaces
    - Reason: Sass has some flexibility on selector nesting, and an indentation
      of 4 spaces makes the nesting visually distinct.

- Avoid nesting selectors more than 3 levels deep.
    - Reason: nesting can make styles harder to follow as they list farther
      away from the top selector.

- Use classes. Please avoid tags (`p`), and do not use ids (`#element`).
    - Reason: tags tend to become nested several layers deep in order to reach
      into an element for styling. Ids add specificity that cannot be easily
      overridden in a controlled way later in the code.

- Use the modifier syntax `.m--[name]` to specify styles for different states,
  e.g. `.m--active` for active links.
    - Reason: the modifier extends an existing element's styling and can be
      reused.

- Use pseudo-selectors sparingly. Exceptions: input and link pseudo-selectors
  to describe state that is otherwise difficult to do without Javascript, such
  as `:hover` and `:enabled`.
    - Reason: position pseudo-selectors (`:nth-child(n)`) can have unexpected
      effects depending on the order they were added. Use a modifier class
      instead.

- Comments: for development, use `//` for Sass functions and variables.
    - Reason: easier to skim CSS output when debugging. Comments with `//` do
      not get printed out to CSS, while comments between `/* */` do, so using
      `//` hides comments for Sass with no CSS line output like functions. In
      production, the CSS will be compressed with all comments stripped, so
      using either is fine as long as it is applied consistently.

## Images

- Export .png at resolution x4 of the canvas size, e.g. at 384dpi in
  Inkscape.
    - Reason: support x2 zoom on mobile devices. This improves legibility
      for people with weak vision.

- Compress .png with a utility like pngquant when preparing for production.
    - Reason: reduce load times.

- SVG is fine, but preview them on a few mobile browsers before using.
    - Reason: some SVG images may not display correctly on certain mobile
      browsers.
