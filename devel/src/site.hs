------------------------------------------------------------------------------
{-# LANGUAGE OverloadedStrings #-}
import           Hakyll

------------------------------------------------------------------------------
-- | Change default host IP and static HTML output directory
config :: Configuration
config = defaultConfiguration
    { destinationDirectory = "public"
    , previewHost = "0.0.0.0"
    }

main :: IO ()
main = hakyllWith config $ do
    match "static/**" $ do
        route idRoute
        compile copyFileCompiler

    match "templates/main/*.html" $ compile templateCompiler

    match "templates/page/*.css" $ do
        -- Route templates/page/*.html -> *.html
        route $ gsubRoute "templates/page/" (const "")
        compile copyFileCompiler

    match "templates/page/*.html" $ do
        -- Route templates/page/*.html -> *.html
        route $ gsubRoute "templates/page/" (const "")
        compile $ do
            getResourceBody
                >>= applyAsTemplate defaultContext
                >>= loadAndApplyTemplate "templates/main/main.html" defaultContext
                >>= relativizeUrls
