#!/bin/bash
# Description: wrapper script around some commands for convenience.
_script="./build.sh"


# Template paths
template_dir="templates"
wt_watch_dir="page"
hamlet_dir="hamlet"

# Project name and paths
proj="ui-prototype"
bin="$(pwd)/devel/bin"
bin_repo="https://framagit.org/snowdrift/ui-prototype-bin"

# Executable paths
app="$bin/$proj"
wt="$bin/wt"
html2hamlet="$bin/html2hamlet"

# If a custom build config exists, source it
test -f .build && source .build


setup() {
    # Check if bin directory exists and purge contents
    if [ -d $bin ]; then rm -rf $bin/*
    else mkdir $bin; fi
    # Check distro and download prebuilt binaries
    distro=$(uname -o)
    arch=$(uname -i)
    if [ "$distro" == "GNU/Linux" ] && [ "$arch" == "x86_64" ]; then
        echo "Downloading prebuilt binaries ..."
        cd $bin
        curl -LO $bin_repo/raw/master/amd64/ui-prototype
        chmod +x $app
        sleep 2
        curl -LO $bin_repo/raw/master/amd64/html2hamlet
        chmod +x $html2hamlet
        sleep 2
        curl -LO $bin_repo/raw/master/amd64/wt
        chmod +x $wt
        echo "Done."
    else
        echo "Sorry, prebuilt binaries are not available for your platform."
        echo "Please see the README for instructions to build from source."
    fi
}

sass_compile() {
    echo "Preprocessing Sass ..."
    cd $template_dir
    $wt compile -proj $wt_watch_dir -b $wt_watch_dir
    cd ..
}

sass_watch() {
    echo "Watching for Sass changes ..."
    cd $template_dir
    $wt watch -proj $wt_watch_dir -b $wt_watch_dir
    cd ..
}

static_compile() {
    echo "Generating static site ..."
    sass_compile
    $app rebuild
}

# Checks whether a hamlet file already exists and renames it with timestamp
backup_hamlet() {
  if [ -f "$hamlet_dir/$1.hamlet" ]; then
      timestamp=$(date "+%Y%m%d-%H%M%S")
      mv "$hamlet_dir/$1.hamlet" "$hamlet_dir/$1-$timestamp.hamlet"
  fi
}

# Covnert HTML to Hamlet with html2hamlet
convert_to_hamlet() {
    # Check whether the path is a file or directory
    if [[ -d "$1" ]]; then
        for file in $(ls "$1"); do
            ext="${file##*.}"
            base_name="${file%.*}"
            if [ "$ext" == "html" ]; then
                echo "Converting $file to Hamlet ..."
                $html2hamlet "$1/$file"
                # If file exists, make backup
                backup_hamlet "$base_name"
                mv "$1/$base_name.hamlet" "$hamlet_dir/"
            fi
        done
    else
        echo "Converting $1 to Hamlet ..."
        base_name="${1%.*}"
        $html2hamlet "$1"
        backup_hamlet "$base_name"
        mv "$base_name.hamlet" "$hamlet_dir/"

    fi
}

watch() {
    $app watch &
    sass_watch
}

case "$1" in
    *help)
        cat << EOF
Usage: $_script gen|hamlet|help|rebuild|server|setup|watch

gen         Generate static files only, e.g. for GitLab Pages

hamlet      Convert HTML file or directory of HTML files to Hamlet
            e.g. $_script hamlet path/to/page.html

help        Show this help message

rebuild     Re-compile app and generate static files
            (Requires Stack. Only run after Haskell source changes.)

server      Preview server

setup       Set up prototype (run this first!)

watch       Start server, watching for HTML/Sass changes
EOF
        ;;
    build)
        stack build
        static_compile
        ;;

    gen) static_compile;;

    hamlet) convert_to_hamlet "$2";;

    server) $app server;;

    setup) setup;;

    *) watch;;
esac
