# README

UI prototype for snowdrift, built with Hakyll and Wellington.

License: AGPL3+


## Install

- Fork the repository and download a copy of your fork:

  ```
  git clone https://framagit.org/username/ui-prototype
  ```

- Change into the directory and run the setup command.

  >Note: the prototype setup currently fetches prebuilt Linux amd64 binaries as
  >a convenience. If you are using another platform or architecture, see the
  >next section for instructions to build from source.

  ```
  cd ui-prototype
  ./build.sh setup
  ```

- Build the static site and start watching for changes: `./build.sh`


### Building from source

- Install [Stack].

- Compile [Wellington], then place the executable in the `devel/bin`
  subdirectory.

- Run `stack build` to install [Hakyll] and compile the project app. Add a file
  called `.build` in the top-level directory with the path to the project app,
  e.g.:

  ```
  app=$(pwd)/.stack-work/install/x86_64-linux/lts-9.18/8.0.2/bin/ui-prototype
  ```

- Start the server: `./build.sh`

- To rebuild after Haskell source changes: `./build.sh rebuild`


## Workflow tips

- To see command options: `./build.sh help`

- If you have access to the shared prototype repo, you may want to add the
  shared repo as a location on your local forked copy, so you can push to both
  your fork and the shared repo:

  ```
  git remote add snowdrift https://framagit.org/snowdrift/ui-prototype
  ```

  Then you can send your changes like this:

  ```
  // Push to your fork (master branch)
  git push origin master

  // Push to shared repo (master branch)
  git push snowdrift master
  ```


## Credits

Images include adaptations of [Mimi and Eunice] characters by Nina Paley.


[Stack]: https://docs.haskellstack.org/en/stable/README/
[Wellington]: https://github.com/wellington/wellington
[Hakyll]: https://jaspervdj.be/hakyll/
[Mimi and Eunice]: http://mimiandeunice.com/
